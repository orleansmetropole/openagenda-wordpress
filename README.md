# OpenAgenda Wordpress Plugin #

Objectif : simple plugin qui permet l'affichage d'un agenda hébergé sur la plateform SaaS [OpenAgenda](https://openagenda.com)

## Usage ##

Insérer simplement le shortcode `[oa_list_by_agenda agenda_id="xxxxxxxxxxx"]` dans un post.
Ce shortcode permet de gérer l'affichage de la liste et du single event.
 
## Require ##

* Wordpress 3.6+
* Composer

## TODO ##

* Refactoring to POO
* Dynamic permastruct
