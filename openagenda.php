<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://bitbucket.org/orleansagglo/openagenda-wordpress
 * @since             1.0.0
 * @package           Openagenda
 *
 * @wordpress-plugin
 * Plugin Name:       Open Agenda WordPress Plugin Reader
 * Plugin URI:        https://bitbucket.org/orleansagglo/openagenda-wordpress
 * Description:       Simple reader from OpenAgenda Export.json. Depends Open Agenda PHP SDK
 * Version:           1.0.0
 * Author:            Orléans et son Agglo
 * Author URI:        https://bitbucket.org/orleansagglo/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       openagenda
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once 'vendor/autoload.php';


/**
 * OpenAgenda shortcode integration
 *
 * params :
 *  - agenda_id : ID
 *  - per_page : number of post
 *
 * @param $atts
 * @return bool|void
 * @throws Exception
 */
function oa_list_by_agenda( $atts ) {
	$a = shortcode_atts( array(
		'agenda_id' => false,
		'per_page' => 5,
	), $atts );

	if (empty($a['agenda_id'])) {
        _doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', 'agenda_id args' ), '1.0.0' );
        return false;
    }

    $oa = new OpenAgenda\Reader($a['agenda_id']);

//    debug(get_query_var('page'),1);

    if ($id = get_query_var('oa_id')) {
        return oa_get_template( 'event.php', [
            'event' => $oa->getEvent($id)
        ] );
    } else {
        $response = $oa->getEvents([
            'page' => get_query_var('page') ? get_query_var('page') : 1
        ]);
        return oa_get_template( 'events.php', [
            'events' => $response->events,
            'pagination' => $response->pagination,
            'total' => $response->total
        ] );
    }

}
add_shortcode( 'oa_list_by_agenda', 'oa_list_by_agenda' );

/**
 * Locate template.
 *
 * Locate the called template.
 * Search Order:
 * 1. /themes/theme/openagenda/$template_name
 * 2. /themes/theme/$template_name
 * 3. /plugins/openagenda/templates/$template_name.
 *
 * @since 1.0.0
 *
 * @param 	string 	$template_name			Template to load.
 * @param 	string 	$string $template_path	Path to templates.
 * @param 	string	$default_path			Default path to template files.
 * @return 	string 							Path to the template file.
 */
function oa_locate_template( $template_name, $template_path = '', $default_path = '' ) {

	// Set variable to search in woocommerce-plugin-templates folder of theme.
	if ( ! $template_path ) :
		$template_path = 'openagenda/';
	endif;

	// Set default plugin templates path.
	if ( ! $default_path ) :
		$default_path = plugin_dir_path( __FILE__ ) . 'templates/'; // Path to the template folder
	endif;

	// Search template file in theme folder.
	$template = locate_template( array(
		$template_path . $template_name,
		$template_name
	) );

	// Get plugins template file.
	if ( ! $template ) :
		$template = $default_path . $template_name;
	endif;

	return apply_filters( 'oa_locate_template', $template, $template_name, $template_path, $default_path );

}

/**
 * Get template.
 *
 * Search for the template and include the file.
 *
 * @since 1.0.0
 *
 * @see wcpt_locate_template()
 *
 * @param string 	$template_name			Template to load.
 * @param array 	$args					Args passed for the template file.
 * @param string 	$string $template_path	Path to templates.
 * @param string	$default_path			Default path to template files.
 */
function oa_get_template( $template_name, $args = array(), $template_path = '', $default_path = '' ) {

    if ( is_array( $args ) && isset( $args ) ) :
        extract( $args );
    endif;

    $template_file = oa_locate_template( $template_name, $template_path, $default_path );

    if ( ! file_exists( $template_file ) ) :
        _doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', $template_file ), '1.0.0' );
        return;
    endif;

    include $template_file;

}

/**
 * Init rewriting rules
 *
 *
 */
function add_openagenda_rules() {

    add_rewrite_tag('%event_slug%','([^/]*)', 'event_slug=');
    add_rewrite_tag('%oa_id%','([^/]*)', 'oa_id=');

    add_permastruct('event', '%pagename%/event/%oa_id%/%event_slug%', [
        'with_front' => false,
        'ep_mask' => EP_PAGES
    ]);

}
add_action( 'init', 'add_openagenda_rules' );

/**
 * Add specific query vars
 *
 * @param $vars
 * @return array
 */
function add_query_vars_filter( $vars ){
    $vars[] = 'event_slug';
    $vars[] = 'oa_id';
    return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

/**
 * Get an event permalink
 *
 * @param $slug
 * @param $oid
 * @return string
 */
function get_oa_event_permalink( $slug, $oid ){

    global $wp_rewrite, $post;

    $p = $wp_rewrite->extra_permastructs['event']['struct'];
//    debug($p,1);
    return '/'.str_replace([
        '%pagename%',
        '%oa_id%',
        '%event_slug%'
    ], [
        $post->post_name,
        $oid,
        $slug
    ], $p);

}