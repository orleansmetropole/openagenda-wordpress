
<?php foreach ($events as $event) : //debug($event,1) ?>

    <h2><?php echo $event->title->fr ?></h2>
    <p><?php echo $event->range->fr ?> - <?php echo $event->locationName ?></p>
    <p><img src="<?php echo $event->thumbnail ?>" /></p>
    <p><?php echo $event->description->fr ?></p>
    <p><a href="<?php echo get_oa_event_permalink($event->slug,$event->uid) ?>">Lire la suite</a></p>

<?php endforeach; ?>

<ul>
<?php foreach ($pagination as $page ) : ?>
    <li>
        <?php if ($page->isIsclickable()) : ?>
            <a href="<?php echo add_query_arg(array(
            'page' => $page->getNumber()
        ), get_permalink()) ?>"><?php echo $page->getType().' '.$page->getNumber() ?></a>
        <?php else : ?>
            <?php echo $page->getType().' '.$page->getNumber() ?>
        <?php endif; ?>

    </li>
<?php endforeach; ?>
</ul>